<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit2.php");

use \PHPUnit\Framework\TestCase;
use TypeError;
use WhiteRabbit2;

class WhiteRabbit2Test extends TestCase
{
    /** @var WhiteRabbit2 */
    private $whiteRabbit2;
    
    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit2 = new WhiteRabbit2();
    }
    
    /**
     * @dataProvider cashProvider
     *
     * @param array $expected Expected array result
     * @param int $amount Amount to test
     */
    public function testCash($expected, $amount)
    {
        $this->assertEquals($expected, $this->whiteRabbit2->findCashPayment($amount));
    }
    
    /**
     * Checks that if a string value is passed to the function the type hint error will be raised
     */
    public function testCashTypeHint()
    {
        $this->expectException(TypeError::class);
        $this->whiteRabbit2->findCashPayment('not an int value');
    }
    
    public function cashProvider()
    {
        $test1 = [
            [
                '1' => 1,
                '2' => 0,
                '5' => 1,
                '10' => 0,
                '20' => 1,
                '50' => 0,
                '100' => 0,
            ],
            26,
        ];
        
        $test2 = [
            [
                '1' => 0,
                '2' => 0,
                '5' => 0,
                '10' => 0,
                '20' => 0,
                '50' => 0,
                '100' => 0
            ],
            0,
        ];
        
        $test3 = [
            [
                '1' => 0,
                '2' => 2,
                '5' => 1,
                '10' => 1,
                '20' => 0,
                '50' => 0,
                '100' => 0
            ],
            19,
        ];
        
        $test4 = [
            [
                '1' => 0,
                '2' => 1,
                '5' => 0,
                '10' => 1,
                '20' => 1,
                '50' => 0,
                '100' => 0
            ],
            32,
        ];
        
        $test5 = [
            [
                '1' => 1,
                '2' => 1,
                '5' => 1,
                '10' => 1,
                '20' => 1,
                '50' => 1,
                '100' => 1
            ],
            188,
        ];
        
        $test6 = [
            [
                '1' => 0,
                '2' => 0,
                '5' => 0,
                '10' => 0,
                '20' => 0,
                '50' => 0,
                '100' => 0
            ],
            -188,
        ];
        
        return [$test1, $test2, $test3, $test4, $test5, $test6];
    }
}
