<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use \PHPUnit\Framework\TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;
    
    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();
        
    }
    
    //SECTION FILE !
    
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier)
    {
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }
    
    public function multiplyProvider()
    {
        /**
         * The test will fail for any amount or multiplier has decimals
         * The test will fail for any ($amount * $multiplier) - &amount that is lower than 0.49
         * The test will fail for the amount = 7
         */
        return [
            [4, 2, 2],
            [6, 3, 2],
            [-7, 7, -1],
            [0, 7, 0],
            [0.666, 0.333, 2],
            [0.98, -0.49, 2],
            [0, -5, 0],
        ];
    }
}
