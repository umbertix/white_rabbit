<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit.php");

use \PHPUnit\Framework\TestCase;
use WhiteRabbit;

class WhiteRabbitTest extends TestCase
{
    /** @var WhiteRabbit */
    private $whiteRabbit;
    
    public function setUp()
    {
        $this->whiteRabbit = new WhiteRabbit();
        parent::setUp();
    }
    
    /**
     * @dataProvider medianProvider
     *
     * @param array $expected Expected result
     * @param string file file to test
     *
     * @throws \Exception
     */
    public function testMedian($expected, $file)
    {
        $result = $this->whiteRabbit->findMedianLetterInFile($file);
        $this->assertTrue(in_array($result, $expected));
    }
    
    public function medianProvider()
    {
        return [
            [[["letter" => "m", "count" => 9240], ["letter" => "f", "count" => 9095]], __DIR__ . "/../txt/text1.txt"],
            [[["letter" => "w", "count" => 13333], ["letter" => "m", "count" => 12641]], __DIR__ . "/../txt/text2.txt"],
            [[["letter" => "w", "count" => 2227], ["letter" => "g", "count" => 2187]], __DIR__ . "/../txt/text3.txt"],
            [[["letter" => "w", "count" => 3049]], __DIR__ . "/../txt/text4.txt"],
            [[["letter" => "z", "count" => 858]], __DIR__ . "/../txt/text5.txt"],
        ];
    }
}
