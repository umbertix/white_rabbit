<?php

/**
 * Class WhiteRabbit2
 * Makes use of type hinting, so PHP 7 is a requirement for this class.
 */
class WhiteRabbit2
{
    /**
     * Array of possible coins to be used, it must be strictly decreasing in value
     */
    const COINS = [
        '100',
        '50',
        '20',
        '10',
        '5',
        '2',
        '1',
    ];

    /**
     * Initializes an array for all the coins defined in the class to 0
     *
     * @return array Contains all possible coins defined in the class to the value 0
     */
    private function initCoinsArray()
    {
        $coins_array = [];
        foreach (self::COINS as $coin) {
            $coins_array[$coin] = 0;
        }
        
        return $coins_array;
    }

    /**
     * Return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array will use as few coins as possible.
     *
     * @param int $amount Amount that we must break down in minimal amount of coins
     *
     * @return array
     */
    public function findCashPayment(int $amount)
    {
        $coins = $this->initCoinsArray();
        
        if ($amount <= 0) {
            return $coins;
        }
        
        for ($i = 0; $i < count(self::COINS); $i++) {
            while ($amount >= self::COINS[$i]) {
                $amount = $amount - self::COINS[$i];
                $coins[self::COINS[$i]] += 1;
            }
        }
        
        return $coins;
    }
}