<?php

/**
 * Class WhiteRabbit
 *
 * This class will try to find the median letter for the file provided. Using the number of occurrences per letter
 *
 * Observations:
 *  If different letters appear the same amount of times the result may vary, based on the asort algorithm result.
 *  A median value might not exist, since we are calculating it and it might not match any given position in the array.
 *  In this case we will have to make an arbitrary decision on what to return, an error, the closest position,
 *  just the median value or something else.
 */
class WhiteRabbit
{
    /**
     * This function returns the median letter in the specified file
     *
     * @param string $filePath Path where to load the file from
     *
     * @return array Array containing letter and the number of occurrences
     * @throws Exception
     */
    public function findMedianLetterInFile($filePath)
    {
        return [
            "letter" => $this->findMedianLetter($this->parseFile($filePath), $occurrences),
            "count" => $occurrences,
        ];
    }
    
    /**
     * Return the letter whose occurrences are the median.
     *
     * @param string $parsedFile Parsed content of the file in a string format
     * @param int    $occurrences Sets the median letter occurrences value
     *
     * @return string Median letter
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        // Get the occurrences for each character
        $calculated_array = count_chars($parsedFile, 1);
    
        //Sort the array by the number of occurrences
        asort($calculated_array);

        $numKeys = count($calculated_array);
        $median_position = $numKeys/2;

        if ($this->isEven($numKeys)) {
            $occurrences_array = array_values($calculated_array);
            $median_occurence = ($occurrences_array[$median_position - 1] + $occurrences_array[$median_position]) / 2;

            $position = array_search($median_occurence, $calculated_array);
            if (!$position) {
                $position = $median_position;
//                throw new Exception("The median position calculation ({$median_occurence}) has failed to find a letter to match.");
            }

        } else {
            $position = $median_position;
        }
    
        $iterator = new ArrayIterator($calculated_array);
        $iterator->seek($position);
    
        $occurrences = $iterator->current();
        return chr($iterator->key());
    }
    
    /**
     * Parse the input file for letters.
     *
     * @param string $filePath file path
     *
     * @return string parsed file content
     * @throws Exception Raise exception in case of an error in the function
     */
    private function parseFile($filePath)
    {
        if (empty($filePath) || !file_exists($filePath)) {
            // By using the file_get_contents an E_WARNING level error would be generated if filename cannot be found
            throw new Exception('The file path is required, please provide a valid filePath');
        }
        
        $content = file_get_contents($filePath);
        
        if (!$content) {
            throw new Exception('There is no content on the loaded file.');
        }
        
        return $this->sanitizeString($content);
    }
    
    /**
     * Removed any non-letter character from the given string
     *
     * @param string $string String to be sanitized
     *
     * @return string|string[]|null Sanitized result
     */
    private function sanitizeString($string)
    {
        return strtolower(preg_replace('/[^\p{L}]/u', '', $string));
    }
    
    /**
     * Check if the entered value is odd or even
     *
     * @param int $number
     *
     * @return bool returns true when the input number is even
     */
    private function isEven($number)
    {
        return $number % 2 === 0;
    }
}